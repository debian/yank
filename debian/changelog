yank (1.3.0-1) unstable; urgency=medium

  * New upstream version 1.3.0
  * Bump-up Standards-Version
  * Use debhelper-compat 12
  * d/watch: use version 4
  * d/rules: install man page as yank-cli.1

 -- Sebastien Delafond <seb@debian.org>  Mon, 24 Oct 2022 16:00:27 +0200

yank (1.2.0-1) unstable; urgency=medium

  * New upstream version 1.2.0

 -- Sebastien Delafond <seb@debian.org>  Sat, 21 Dec 2019 10:22:01 +0100

yank (1.1.0-2) unstable; urgency=medium

  * Fix FTCBFS (Closes: #929004), thanks to the patch provided entirely
    by Helmut Grohne <helmut@subdivi.de>
    + Pass INSTALL_PROGRAM without -s
    + Also pass PROG= to dh_auto_build
  * Bump-up Standards-Version

 -- Sebastien Delafond <seb@debian.org>  Wed, 15 May 2019 17:18:55 +0200

yank (1.1.0-1) unstable; urgency=medium

  * New upstream version 1.1.0

 -- Sebastien Delafond <seb@debian.org>  Wed, 07 Nov 2018 16:29:11 +0100

yank (1.0.0-1) unstable; urgency=medium

  * New upstream version 1.0.0

 -- Sebastien Delafond <seb@debian.org>  Sun, 09 Sep 2018 11:16:09 +0200

yank (0.8.3-2) unstable; urgency=medium

  * Switch to DEP 14
  * Update Vcs-* to point to salsa.d.o
  * Bump-up Standards-Version

 -- Sebastien Delafond <seb@debian.org>  Thu, 05 Apr 2018 13:37:10 +0200

yank (0.8.3-1) unstable; urgency=medium

  * Imported Upstream version 0.8.3

 -- Sebastien Delafond <seb@debian.org>  Tue, 04 Jul 2017 14:43:00 +0200

yank (0.8.2-1) unstable; urgency=medium

  * Imported Upstream version 0.8.2
  * Makefile patch integrated by upstream

 -- Sebastien Delafond <seb@debian.org>  Sun, 19 Feb 2017 15:37:53 +0100

yank (0.8.1-1) unstable; urgency=medium

  * Imported Upstream version 0.8.1
  * Upstream now provides a way to install the yank binary under another name

 -- Sebastien Delafond <seb@debian.org>  Sun, 19 Feb 2017 12:17:19 +0100

yank (0.8.0-1) unstable; urgency=medium

  * Imported Upstream version 0.8.0

 -- Sebastien Delafond <seb@debian.org>  Tue, 17 Jan 2017 10:26:30 +0100

yank (0.7.1-1) unstable; urgency=medium

  * Imported Upstream version 0.7.1
  * Fix Makefile's clean target
  * Adjust debian/patches/0001-Makefile-adjustments

 -- Sebastien Delafond <seb@debian.org>  Mon, 10 Oct 2016 10:50:36 +0200

yank (0.7.0-1) unstable; urgency=medium

  * New upstream release

 -- Sebastien Delafond <seb@debian.org>  Thu, 16 Jun 2016 12:02:54 +0200

yank (0.6.2-1) unstable; urgency=medium

  * New upstream version 0.6.2
  * Fix debian/watch

 -- Sebastien Delafond <seb@debian.org>  Fri, 12 Feb 2016 08:46:33 +0100

yank (0.4.1-1) unstable; urgency=medium

  * New upstream release (Closes: #802292)

 -- Sebastien Delafond <seb@debian.org>  Tue, 20 Oct 2015 08:58:54 +0200

yank (0.4.0-2) unstable; urgency=medium

  * Add docs and README.Debian, documenting the fact that this package
    ships /usr/bin/yank-cli because /usr/bin/yank is already taken.

 -- Sebastien Delafond <seb@debian.org>  Fri, 16 Oct 2015 08:07:02 +0200

yank (0.4.0-1) unstable; urgency=low

  * Initial release (Closes: #801837)

 -- Sebastien Delafond <seb@debian.org>  Thu, 15 Oct 2015 15:53:45 +0200
